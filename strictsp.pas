{$I trans.inc}
program macro;
uses testlib;
begin
  StrictEof;
  DisablePE;
  repeat
    if ans.seekeof then break;
    if ouf.seekeof then ouf.UnExpectedEOF;
    if ans.cur<>ouf.cur then
      QUIT (_WA, '��ᮢ������� � ����樨 '+ouf.GetPosPair+': ����� '+
                 SymbolToString (ouf.cur)+' ��������� '+
                 SymbolToString (ans.cur));
    ans.nextchar; ouf.nextchar;
  until false;
  QUIT (_OK, '');
end.